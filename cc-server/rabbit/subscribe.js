const channel = require('./channel');

const subscribe = (ch, exchange, topic, queue, cbData, cb) => {

  let ok = ch.assertQueue(queue, { durable: true, exclusive: false, autoDelete: false });

  ok = ok.then((qok) => {
    const queue = qok.queue;

    return ch.bindQueue(queue, exchange, topic)
      .then(() => {
        return queue;
      });
  });

  ok = ok.then((queue) => {
    return ch.consume(queue, cbData, { autoAck: false, noAck: false });
  });

  return ok.then((e) => {
    cb(topic, e.consumerTag);
  });
};

module.exports = { subscribe };

