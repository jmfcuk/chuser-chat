const amqp = require('amqplib');
const exchangeType = 'topic';

const channel = (conn, exchange, cb) => {

    return conn.createChannel().then((ch) => {

        const ok = ch.assertExchange(exchange, exchangeType, { durable: true, autoDelete: false  });

        return ok.then(() => {
            cb(ch);
        });
    });
}

module.exports = channel;

