const channel = require('./channel');

const publish = (ch, exchange, topic, message) => {

    const msg = JSON.stringify(message);

    ch.publish(exchange, topic, Buffer.from(msg), { persistent: true });
};

module.exports = { publish };


