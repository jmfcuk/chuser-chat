const express = require('express');
const http = require('http');
const socketIo = require('socket.io');
const sspi = require('node-sspi');
const cors = require('cors');
const amqp = require('amqplib');
const index = require('./routes/index');
const channel = require('./rabbit/channel');
const pub = require('./rabbit/publish');
const sub = require('./rabbit/subscribe');

const app = express();
const server = http.createServer(app);

const nsspi = new sspi({
    retrieveGroups: false
});

app.use(cors({
    credentials: true,
    origin: true
}));

app.use('/', (req, res, next) => {
    nsspi.authenticate(req, res, (err) => {
        res.finished || next();
    });
});

app.use(index);

const _RABBIT_URI = 'amqp://admin:admin@hnap70/c-chat';
const _EXCHANGE = 'c-chat-exchange';
const _TICKER_TOPIC = 'ticker';

const _CC_CONNECT = 'connection';
const _CC_DISCONNECT = 'disconnect';
const _CC_MESSAGE = 'cc-message';
const _CC_MESSAGE_ACK = 'cc-message-ack';
const _CC_ROOM_SUBSCRIBE = 'cc-room-subscribe';
const _CC_ROOM_UNSUBSCRIBE = 'cc-room-unsubscribe';
const _CC_PM_REQ = 'cc-pm-req';
const _CC_PM_CANCEL = 'cc-pm-cancel';

const _MESSAGETYPE_TICKER = 0;
const _MESSAGETYPE_MESSAGE = 1;
const _MESSAGETYPE_PM_REQUEST = 2;
const _MESSAGETYPE_PM_CANCEL = 3;

const _TICK_TIME = 5000;

let interval = null;

amqp.connect(_RABBIT_URI).then((conn) => {

    channel(conn, _EXCHANGE, (tickCh) => {

        if (interval) {
            clearInterval(interval);
        }

        interval = setInterval(() => {
            pub.publish(tickCh, _EXCHANGE, 'ticker',
                {
                    tag: uuid4(),
                    type: _MESSAGETYPE_TICKER,
                    time: Date(),
                    user: {
                        name: 'system',
                        loginTime: Date(0, 0, 0),
                        domain: '.',
                        chatName: 'system',
                        subscribedRooms: []
                    },
                    room: 'ticker',
                    message: 'tick'
                });

        }, _TICK_TIME);
    });

    const io = socketIo(server);

    io.on(_CC_CONNECT, (socket) => {
        console.log('client connected');
        let unackedMessages = [];

        channel(conn, _EXCHANGE, (subCh) => {
            channel(conn, _EXCHANGE, (pubCh) => {
                channel(conn, _EXCHANGE, (pmCh) => {

                    let topicToTag = {};

                    socket.on(_CC_DISCONNECT, () => {
                        console.log('client disconnected');
                        pubCh.close();
                        subCh.close();
                    });

                    socket.on(_CC_ROOM_SUBSCRIBE, (data) => {
                        console.log('%s subscribe %s', data.user, data.room);
                        sub.subscribe(subCh, _EXCHANGE,
                            data.room,
                            data.user,
                            (msg) => {
                                let m = msg.content.toString();
                                let message = JSON.parse(m);
                                if (message.type != _MESSAGETYPE_TICKER) {
                                    console.log('forwarding %s: %s > %s: (%s)',
                                        message.user.chatName, message.room, message.message, message.tag);
                                }
                                socket.emit(_CC_MESSAGE, message);
                                unackedMessages[message.tag] = msg;
                            },
                            (topic, tag) => {
                                console.log('topicToTag[%s]=%s', topic, tag);
                                topicToTag[topic] = tag;
                            });
                    });

                    socket.on(_CC_ROOM_UNSUBSCRIBE, (data) => {
                        console.log('%s unsubscribe %s (%s)',
                            data.user, data.room, topicToTag[data.room]);

                        const tag = topicToTag[data.room];
                        if (tag) {
                            subCh.cancel(tag);
                            delete topicToTag[data.room];
                        }
                    });

                    socket.on(_CC_MESSAGE, (msg) => {
                        console.log('%s > %s: %s (%s)', msg.user.chatName, msg.room, msg.message, msg.tag);
                        pub.publish(pubCh, _EXCHANGE, msg.room, msg);
                    });

                    socket.on(_CC_MESSAGE_ACK, (data) => {
                        const msg = unackedMessages[data.tag];
                        if (msg) {
                            if (msg.type !== _MESSAGETYPE_TICKER) {
                                console.log('%s ack %s', data.user, data.tag);
                            }
                            subCh.ack(msg);
                        }
                    });

                    socket.on(_CC_PM_REQ, (data) => {
                        console.log('%s pm req > %s', data.reqUser, data.targetUser);
                        pub.publish(pmCh, 
                                    _EXCHANGE, 
                                    '', 
                                    {});
                    });

                    socket.on(_CC_PM_CANCEL, (data) => {
                        console.log('%s cancel pm > %s', data.userCancelling, data.otherUser);
                    });
                }); // pub channel
            }); // sub channel
        }); // pm channel
    }); // socket io connection
}); // rabbit connection

const uuid4 = () => {

    var d = new Date().getTime();

    if (typeof performance !== 'undefined' && typeof performance.now === 'function') {
        d += performance.now();
        return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function (c) {
            var r = (d + Math.random() * 16) % 16 | 0;
            d = Math.floor(d / 16);
            return (c === 'x' ? r : (r & 0x3 | 0x8)).toString(16);
        });
    };
}

const port = process.env.PORT || 4000;
server.listen(port, () => console.log(`Listening on port ${port}`));
