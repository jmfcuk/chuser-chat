const express = require("express");
const router = express.Router();

router.get('/', (req, res) => {

  let user = req.connection.user;

  if (user) {

    let domain = '.';
    let name = 'unauthorized';
    const arr = user.split('\\');
    if (arr.length > 1) {
      domain = arr[0];
      name = arr[1];
    } else {
      name = user;
    }

    console.log('login - %s ', name);

    res.send({
      domain: domain, name: name, chatName: name,
      loginTime: new Date(), subscribedRooms: []
    }).status(200);
  }
  else
    res.send('unauthorized').status(401);
});

module.exports = router;
