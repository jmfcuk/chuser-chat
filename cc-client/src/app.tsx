import * as React from 'react';
import { Grid, Col } from 'react-bootstrap';
import * as SocketIOClient from 'socket.io-client';
import { UserDef, MessageType, MessageDef } from './app.d';
import * as CcMsgs from './bits/cc-msgs';
import * as Defaults from './defaults';
import login from './bits/login';
import uuid4 from './bits/uuid';
import Header from './components/header';
import ChangeChatName from './components/change-chatname';
import SubscribedRoomList from './components/subscribed-room-list';
import IncomingList from './components/incoming-list';
import SayIt from './components/sayit';
import './styles.css';

interface AppProps { }

interface AppState {
  user: UserDef;
  currentRoom: string;
  incoming: Array<MessageDef>;
  changeChatName: boolean;
  private: boolean;
}

class App extends React.Component<AppProps, AppState> {

  state: AppState;
  socket: SocketIOClient.Socket;

  constructor(props: AppProps) {
    super(props);

    this.state = {
      user: Defaults._DEF_USER,
      currentRoom: '',
      incoming: [],
      changeChatName: false,
      private: false
    };

    this.socket = SocketIOClient(Defaults._DEF_SVC_URL);
  }

  componentDidMount() {

    login(Defaults._DEF_SVC_URL, (u: UserDef) => {
      this.setState({ user: u }, () => {
        this.socket.on(CcMsgs._CC_MESSAGE, (msg: MessageDef) => this.onMessageIn(msg));
      });
    });
  }

  onChangeChatName = () => {

    this.setState({ changeChatName: true });
  }

  onSaveChatName = (chatname: string) => {
    let u = this.state.user;
    u.chatName = chatname;
    this.setState({ user: u, changeChatName: false });
  }

  onToggleRoomSub = (room: string, sub: boolean) => {

    let usr = this.state.user;
    let croom = '';
    let subAction: string = CcMsgs._CC_ROOM_UNSUBSCRIBE;

    if (sub === true) {
      subAction = CcMsgs._CC_ROOM_SUBSCRIBE;
      if (usr.subscribedRooms.indexOf(room) < 0) {
        usr.subscribedRooms.push(room);
        croom = room;
      }
    } else {
      usr.subscribedRooms = usr.subscribedRooms.filter((r) => {
        return r !== room;
      });
      if (usr.subscribedRooms && usr.subscribedRooms.length > 0) {
        croom = usr.subscribedRooms[0];
      }
    }

    this.socket.emit(subAction, { user: usr.chatName, room: room });
    this.setState({ user: usr, currentRoom: croom });
  }

  onToggleTicker = (tick: boolean) => {

    this.onToggleRoomSub(CcMsgs._TICKER_TOPIC, tick);
  }

  onMessageIn = (msg: MessageDef) => {

    let ic = this.state.incoming;
    const mt = msg.type;
    if (mt === MessageType.roomMessage || mt === MessageType.ticker) {
      ic.unshift(msg);
      if (ic.length > Defaults._MAX_INCOMING) {
        ic = ic.slice(0, Defaults._MAX_INCOMING);
      }
      this.setState({ incoming: ic }, () => {
        this.socket.emit(CcMsgs._CC_MESSAGE_ACK, { user: this.state.user.chatName, tag: msg.tag });
      });
    } else if (msg.type === MessageType.pmRequest) {
      confirm(msg.user.chatName + 'has requested pm.');
    } else if (msg.type === MessageType.pmCancel) {
      alert(msg.user.chatName + ' has closed pm.');
    }
  }

  onSendMessage = (msg: string) => {

    if (msg && this.state.currentRoom &&
      this.state.currentRoom !== '') {

      let m: MessageDef = {
        tag: uuid4(),
        type: MessageType.roomMessage,
        time: new Date(),
        user: this.state.user,
        room: this.state.currentRoom,
        message: msg
      };

      this.socket.emit(CcMsgs._CC_MESSAGE, m);
    }
  }

  onRoomSelect = (e: any) => {
    this.setState({ currentRoom: e.target.value });
  }

  onReqPm = (usr: UserDef) => {
    this.socket.emit(CcMsgs._CC_PM_REQ,
      {
        reqUser: this.state.user.chatName,
        targetUser: usr.chatName
      });
  }

  render(): JSX.Element {

    let jsx = <div className="text-center">Unauthorized</div>;

    if (this.state.user.name && this.state.user.name !== Defaults._DEF_USER.name) {
      jsx = (
        <div>
          <Grid>
            <Header
              user={this.state.user}
              rooms={Defaults._DEF_ROOMS}
              onChangeChatName={this.onChangeChatName}
              onToggleRoomSub={this.onToggleRoomSub}
              onToggleTicker={this.onToggleTicker}
            />
            <Col xs={2}>
              <SubscribedRoomList
                rooms={this.state.user.subscribedRooms}
                currentRoom={this.state.currentRoom}
                onRoomSelect={this.onRoomSelect}
              />
            </Col>
            <Col xs={10}>
              <IncomingList
                incoming={this.state.incoming.filter(msg => msg.room === this.state.currentRoom)}
                onClick={this.onReqPm}
              />
              <SayIt onSend={this.onSendMessage} />
            </Col>
            <ChangeChatName
              currentChatName={this.state.user.chatName}
              onSaveChange={this.onSaveChatName}
              visible={this.state.changeChatName}
              onCancel={() => { this.setState({ changeChatName: false }); }}
              onSave={this.onSaveChatName}
            />
          </Grid>
        </div>
      );
    }

    return jsx;
  }
}

export default App;
