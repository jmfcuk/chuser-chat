import { UserDef } from './app.d';

export const _DEF_USER: UserDef = {
    loginTime: new Date(), name: 'unauthorized', domain: '.',
    chatName: 'unauthorized', subscribedRooms: []
};
export const _DEF_ROOMS = ['Room 1', 'Room 2', 'Room 3', 'Room 4', 'Room 5', 'Room 6'];
// export const _DEF_SVC_URL = 'http://10.200.30.181:4000';
export const _DEF_SVC_URL = 'http://localhost:4000';
export const _MAX_INCOMING = 500;

export default {
    _DEF_USER,
    _DEF_ROOMS,
    _DEF_SVC_URL,
    _MAX_INCOMING
};
