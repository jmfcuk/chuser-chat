import * as React from 'react';
import {
  Row,
  Col,
  Form,
  FormGroup,
  FormControl
} from 'react-bootstrap';

interface SayItProps {
  onSend: (msg: string) => void;
}

interface SayItState {
  msg: string;
}

class SayIt extends React.Component<SayItProps, SayItState> {

  state: SayItState;

  constructor(props: SayItProps) {
    super(props);

    this.state = {
      msg: ''
    };
  }

  onChange = (e: any) => {
    this.setState({ msg: e.target.value });
  }

  onSend = (): void => {
    this.props.onSend(this.state.msg);
    this.setState({msg: ''});
  }

  render(): JSX.Element {
    return (
      <Row>
        <Form onSubmit={(e) => e.preventDefault()}>
          <FormGroup>
            <Col xs={10}>
              <FormControl
                type="text"
                value={this.state.msg}
                placeholder="Enter message"
                onChange={this.onChange}
              />
            </Col>
            <Col xs={2}>
              <FormControl
                type="button"
                value="Send"
                onClick={this.onSend}
              />
            </Col>
          </FormGroup>
        </Form>
      </Row>
    );
  }
}

export default SayIt;
