import * as React from 'react';
import { Row, Col, Button } from 'react-bootstrap';
import { UserDef } from '../app.d';

interface HeaderProps {
  user: UserDef;
  rooms: Array<string>;
  onChangeChatName: () => void;
  onToggleRoomSub: (room: string, sub: boolean) => void;
  onToggleTicker: (state: any) => void;
}

const Header = (props: HeaderProps) => {
  return (
    <div className="text-center">
      <Row>
        <Col xs={12}>
          <div className="title-text"><h1><i>Chuser Chat</i></h1></div>
        </Col>
      </Row>
      <Row>
        <h2>Signed in as: {props.user.domain}\{props.user.name}. </h2>
      </Row>
      <Row>
        <Row>
          <h3>Click on a room to toggle subscription.</h3>
          <h4>
            <span>
              <i>Subscribe to Ticker</i>
              <input
                type="checkbox"
                onChange={e => props.onToggleTicker(e.target.checked)}
              />
            </span>
          </h4>
        </Row>
        {props.rooms.map((room) => {
          return (
            <Col
              key={room}
              xs={2}
              className={props.user.subscribedRooms.indexOf(room) > -1 ?
                'subscribed-room' : 'unsubscribed-room'}
            >
              <Button
                className={props.user.subscribedRooms.indexOf(room) > -1 ?
                  'subscribed-room' : 'unsubscribed-room'}
                onClick={() => props.onToggleRoomSub(room, props.user.subscribedRooms.indexOf(room) < 0)}
              >
                {room}
              </Button>
            </Col>
          );
        })}
      </Row>
    </div>
  );
};

export default Header;

