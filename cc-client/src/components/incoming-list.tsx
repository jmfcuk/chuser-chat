import * as React from 'react';
import { UserDef } from '../app.d';

import {
    Row,
    ListGroup,
    ListGroupItem
} from 'react-bootstrap';

interface IncomingListProps {
    incoming: Array<any>;
    onClick: ((usr: UserDef) => void);
}

const IncomingList = (props: IncomingListProps) => {
    return (
        <Row>
            <div className="incoming-messages-list">
                <ListGroup>
                    {props.incoming.map((msg: any, idx: number) => {
                        return (
                            <ListGroupItem 
                                key={idx} 
                                header={msg.user.chatName}
                                onClick={e => props.onClick(msg.user)}    
                            >
                                <div>
                                    { new Date(msg.time).toLocaleDateString() +
                                      new Date(msg.time).toLocaleTimeString() }
                                </div>
                                {msg.message}
                            </ListGroupItem>);
                    })}
                </ListGroup>
            </div>
        </Row>
    );
};

export default IncomingList;

