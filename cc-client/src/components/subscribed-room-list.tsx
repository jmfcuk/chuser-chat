import * as React from 'react';
import {
    Row,
    ListGroup,
    ListGroupItem
} from 'react-bootstrap';


interface RoomListProps {
    rooms: Array<string>;
    currentRoom: string;
    onRoomSelect: (e: any) => void;
}

const SubscribedRoomList = (props: RoomListProps) => {
    return (
        <div>
            <Row className="room-list">
                <ListGroup>
                    {props.rooms.map((room) => {
                        return (
                            <ListGroupItem
                                key={room}
                                value={room}
                                active={props.currentRoom === room}
                                onClick={props.onRoomSelect}
                            >
                                {room}
                            </ListGroupItem>);
                    })}
                </ListGroup>
            </Row>
            <Row className="text-center">
                {props.currentRoom}
            </Row>
        </div>
    );
};

export default SubscribedRoomList;
