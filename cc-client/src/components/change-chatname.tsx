import * as React from 'react';
import { Modal, Button } from 'react-bootstrap';

interface ChangeChatNameProps {
    currentChatName: string;
    onSaveChange: (chatname: string) => void;
    visible: boolean;
    onCancel: () => void;
    onSave: (chatname: string) => void;
}

interface ChangeChatNameState {
    chatName: string;
}

class ChangeChatName extends React.Component<ChangeChatNameProps, ChangeChatNameState> {

    state: ChangeChatNameState;

    constructor(props: ChangeChatNameProps) {
        super(props);

        this.state = {
            chatName: props.currentChatName
        };
    }

    onNameChange = (e: any) => {

        const name = e.target.value;

        if (name) {
            this.setState({ chatName: name });
        }
    }

    onSave = () => {
        this.props.onSave(this.state.chatName);
    }

    render(): JSX.Element {

        return (
            <Modal show={this.props.visible} onHide={this.props.onCancel}>
                <Modal.Header closeButton={true}>
                    <Modal.Title>Change Chat Name</Modal.Title>
                </Modal.Header>
                <Modal.Body>
                    <input
                        type="text" 
                        id="name" 
                        className="text-center"
                        defaultValue={this.state.chatName}
                        onChange={this.onNameChange}
                    />
                </Modal.Body>
                <Modal.Footer>
                    <Button onClick={this.onSave}>Save</Button>
                </Modal.Footer>
            </Modal>
        );
    }
}

export default ChangeChatName;