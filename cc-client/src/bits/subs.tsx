const _SUB_PREFIX = 'c-chat-subs-';

export const saveSubs = (chatName: string, subs: Array<string>, cb?: () => void) => {

    localStorage.setItem(_SUB_PREFIX + chatName, JSON.stringify(subs));

    if (cb) {
        cb();
    }
};

export const loadSubs = (chatName: string, cb: (subs: Array<string>) => void) => {

    let s: string | null = localStorage.getItem(_SUB_PREFIX + chatName);
    let subs: Array<string> = [];
    if (s) {
        subs = JSON.parse(s);
    }
    cb(subs);
};

export default { saveSubs, loadSubs };

