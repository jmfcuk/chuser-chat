import { UserDef } from '../app.d';

const login = (uri: string, cb: (u: UserDef) => void) => {
    fetch(uri, { credentials: 'include' })
        .then((resp) => { return resp.json(); })
        .then((u) => {
            cb(u);
        }).catch((reason) => console.log(reason));
};

export default login;

