export const _CC_MESSAGE = 'cc-message';
export const _CC_MESSAGE_ACK = 'cc-message-ack';
export const _CC_ROOM_SUBSCRIBE = 'cc-room-subscribe';
export const _CC_ROOM_UNSUBSCRIBE = 'cc-room-unsubscribe';
export const _TICKER_TOPIC = 'ticker';
export const _CC_PM_REQ = 'cc-pm-req';
export const _CC_PM_CANCEL = 'cc-pm-cancel';

export default {
    _CC_MESSAGE,
    _CC_MESSAGE_ACK,
    _CC_ROOM_SUBSCRIBE,
    _CC_ROOM_UNSUBSCRIBE,
    _TICKER_TOPIC,
    _CC_PM_REQ,
    _CC_PM_CANCEL
};

