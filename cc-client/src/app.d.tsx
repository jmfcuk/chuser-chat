export interface UserDef {

    loginTime: Date;
    domain: string;
    name: string; 
    chatName: string;
    subscribedRooms: Array<string>;
}

export const enum MessageType {
    ticker = 0,
    roomMessage = 1,
    pmRequest = 2,
    pmCancel = 3
}

export interface MessageDef {
    
    tag: string;
    type: MessageType;
    time: Date;
    user: UserDef;
    room: string;
    message: string;
}

